package xal.app.virtualmachine;

import java.awt.event.ItemEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import xal.extension.jels.smf.impl.EMU;
import xal.extension.jels.smf.impl.NPM;

/**
 *
 * @author benjaminbolling
 */

public class FXMLController implements Initializable {
    public String lastsimtext = null;
    public Double[] sigmaX;
    public Double[] sigmaY;
    public Double[] sigmaZ;
    public Double[] posX;
    public Double[] posY;
    public Double[] positions;
    
    private XYChart.Series[] seriesNPMpos;
    private XYChart.Series[] seriesNPMsigma;
    
    @FXML public Label label;
    @FXML private CheckBox checkboxX;
    @FXML private CheckBox checkboxY;
    @FXML private CheckBox checkboxZ;
    @FXML private CheckBox checkboxBPMs;
    @FXML private CheckBox checkboxVacuumCh;
    
    @FXML private LineChart<Double, Double> plot1;
    @FXML private LineChart<Double, Double> plot2;
    
    @FXML XYChart.Series<Double, Double> seriesSigmaX;
    @FXML XYChart.Series<Double, Double> seriesSigmaY;
    @FXML XYChart.Series<Double, Double> seriesSigmaZ;
    @FXML XYChart.Series<Double, Double> seriesPositions;
    @FXML XYChart.Series<Double, Double> seriesX;
    @FXML XYChart.Series<Double, Double> seriesY;
    @FXML XYChart.Series<Double, Double> seriesXchamber;
    @FXML XYChart.Series<Double, Double> seriesYchamber;
    
    @FXML private void checkboxVacuumChsel(ActionEvent event) throws IOException {
        System.out.println(checkboxVacuumCh.isSelected());
        if(checkboxVacuumCh.isSelected() == true){ plot2.getData().add(seriesXchamber); plot2.getData().add(seriesYchamber); }
        if(checkboxVacuumCh.isSelected() == false){ plot2.getData().removeAll(seriesXchamber); plot2.getData().removeAll(seriesYchamber); }
    }
    @FXML private void checkboxXsel(ActionEvent event) throws IOException {
        System.out.println(checkboxX.isSelected());
        if(checkboxX.isSelected() == true)  { plot1.getData().add(seriesX); plot2.getData().add(seriesSigmaX); }
        if(checkboxX.isSelected() == false) { plot1.getData().removeAll(seriesX); plot2.getData().removeAll(seriesSigmaX); }
    }
    @FXML private void checkboxYsel(ActionEvent event) throws IOException {
        System.out.println(checkboxY.isSelected());
        if(checkboxY.isSelected() == true)  { plot1.getData().add(seriesY); plot2.getData().add(seriesSigmaY); }
        if(checkboxY.isSelected() == false) { plot1.getData().removeAll(seriesY); plot2.getData().removeAll(seriesSigmaY); }
    }
    @FXML private void checkboxZsel(ActionEvent event) throws IOException {
        System.out.println(checkboxZ.isSelected());
        if(checkboxZ.isSelected() == true)  { plot2.getData().add(seriesSigmaZ); }
        if(checkboxZ.isSelected() == false) { plot2.getData().removeAll(seriesSigmaZ); }
    }
    @FXML private void checkboxBPMsel(ActionEvent event) throws IOException {
        System.out.println(checkboxBPMs.isSelected());
        
    }
    
    @FXML private void exportButtonAction(ActionEvent event) throws IOException {
        if (lastsimtext == null) { label.setText("Error: No simulation data to export."); }
        else { // Start exporting
            DirectoryChooser dirchooser = new DirectoryChooser();
            Stage dirwindow = new Stage();
            dirwindow.setTitle("Select simulation data export destination");
            File dirsel = dirchooser.showDialog(dirwindow);
            if (dirsel == null) { label.setText("No exportfolder selected."); }
            else {
                String filepathbasis = new String(dirsel.getAbsolutePath()+"/"+lastsimtext);
                FileWriter write = new FileWriter(filepathbasis+"_siminfo",false);
                PrintWriter print_line = new PrintWriter(write);
                print_line.printf("%s"+"%n", "Simulation datestamp: "+lastsimtext);
                String sequencestr = MainFunctions.mainDocument.getSequence().toString();
                print_line.printf("%s"+"%n", "Simulated section: "+sequencestr);
                double beamcurrent = MainFunctions.mainDocument.getCurrent();
                print_line.printf("%s"+"%n", "Beam current: "+Double.toString(beamcurrent));
                double doubleTotE = MainFunctions.mainDocument.getTotEnergy();
                print_line.printf("%s"+"%n", "Total energy of beam: "+Double.toString(doubleTotE));
                String beamtype = MainFunctions.mainDocument.getProbeType().get();
                print_line.printf("%s"+"%n", "Beam type: "+beamtype);
                String model_sync = MainFunctions.mainDocument.getModel().get();
                print_line.printf("%s"+"%n", "Synchronization type: "+model_sync);
                double alphaX = MainFunctions.mainDocument.getALPHAX();
                print_line.printf("%s"+"%n", "Alpha X: "+Double.toString(alphaX));
                double alphaY = MainFunctions.mainDocument.getALPHAY();
                print_line.printf("%s"+"%n", "Alpha Y: "+Double.toString(alphaY));
                double alphaZ = MainFunctions.mainDocument.getALPHAZ();
                print_line.printf("%s"+"%n", "Alpha Z: "+Double.toString(alphaZ));
                double betaX = MainFunctions.mainDocument.getBETAX();
                print_line.printf("%s"+"%n", "Beta X: "+Double.toString(betaX));
                double betaY = MainFunctions.mainDocument.getBETAY();
                print_line.printf("%s"+"%n", "Beta Y: "+Double.toString(betaY));
                double betaZ = MainFunctions.mainDocument.getBETAZ();
                print_line.printf("%s"+"%n", "Beta Z: "+Double.toString(betaZ));
                double emittX = MainFunctions.mainDocument.getEMITTX();
                print_line.printf("%s"+"%n", "Emittance X: "+Double.toString(emittX));
                double emittY = MainFunctions.mainDocument.getEMITTY();
                print_line.printf("%s"+"%n", "Emittance Y: "+Double.toString(emittY));
                double emittZ = MainFunctions.mainDocument.getEMITTZ();
                print_line.printf("%s"+"%n", "Emittance Z: "+Double.toString(emittZ));
                double x0 = MainFunctions.mainDocument.getX();
                print_line.printf("%s"+"%n", "x0: "+Double.toString(x0));
                double y0 = MainFunctions.mainDocument.getY();
                print_line.printf("%s"+"%n", "y0: "+Double.toString(y0));
                double z0 = MainFunctions.mainDocument.getZ();
                print_line.printf("%s"+"%n", "z0: "+Double.toString(z0));
                double x0p = MainFunctions.mainDocument.getXP();
                print_line.printf("%s"+"%n", "x0': "+Double.toString(x0p));
                double y0p = MainFunctions.mainDocument.getYP();
                print_line.printf("%s"+"%n", "y0': "+Double.toString(y0p));
                double z0p = MainFunctions.mainDocument.getZP();
                print_line.printf("%s"+"%n", "z0': "+Double.toString(z0p));
                double SPACE_CHARGE = MainFunctions.mainDocument.getSCC();
                print_line.printf("%s"+"%n", "Space charge compensation: "+Double.toString(SPACE_CHARGE));
                print_line.printf("%s"+"%n", "Data structure:");
                if(beamtype == "Bunched"){ print_line.printf("%s"+"%n", "position [m], posX [mm], posY [mm], rmsX [mm], rmsY [mm], rmsZ [mm]"); }
                if(beamtype == "DC"){ print_line.printf("%s"+"%n", "position [m], posX [mm], posY [mm], rmsX [mm], rmsY [mm]"); }
                print_line.close();
                
                FileWriter write2 = new FileWriter(filepathbasis+"_data",false);
                PrintWriter print_line2 = new PrintWriter(write2);
                for (int i = 0; i < positions.length; i++){
                    if(beamtype == "Bunched"){ print_line2.printf("%s  %s  %s  %s  %s  %s"+"%n", Double.toString(positions[i]), Double.toString(posX[i]), Double.toString(posY[i]), Double.toString(sigmaX[i]), Double.toString(sigmaY[i]), Double.toString(sigmaZ[i])); } 
                    if(beamtype == "DC"){ print_line2.printf("%s  %s  %s  %s  %s"+"%n", Double.toString(positions[i]), Double.toString(posX[i]), Double.toString(posY[i]), Double.toString(sigmaX[i]), Double.toString(sigmaY[i])); }
                }
                print_line2.close();
            }
        }        
    }
    @FXML private void handleButtonAction(ActionEvent event) throws IOException {
        RunSim runsim = new RunSim();
        runsim.RunSim();
        if(runsim.runflag == 1){
            String beamtype = runsim.beamtype;
            
            // The magnets from the simulated lattice
            System.out.println("Magnets: "+runsim.MagnetNodes);
            System.out.println("Magnet lengths: "+runsim.MagnetLengths);
            System.out.println("Magnet positions: "+runsim.MagnetPositions);
            System.out.println("Magnet types: "+runsim.MagnetTypes);
            // System.out.println("Magnet types: "+runsim.MagnetFields);

            // Create empty xy-chart series
            seriesSigmaX = new XYChart.Series<>();
            seriesSigmaY = new XYChart.Series<>();
            seriesSigmaZ = new XYChart.Series<>();
            seriesPositions = new XYChart.Series<>();
            seriesX = new XYChart.Series<>();
            seriesY = new XYChart.Series<>();
            seriesXchamber = new XYChart.Series<>();
            seriesYchamber = new XYChart.Series<>();
            seriesNPMpos = new XYChart.Series[2];
            seriesNPMsigma = new XYChart.Series[2];

            // arraylists to arrays
            sigmaX = (Double[]) runsim.sigmaX.toArray(new Double[runsim.sigmaX.size()]);
            sigmaY = (Double[]) runsim.sigmaY.toArray(new Double[runsim.sigmaY.size()]);
            sigmaZ = (Double[]) runsim.sigmaZ.toArray(new Double[runsim.sigmaZ.size()]);
            positions = (Double[]) runsim.positions.toArray(new Double[runsim.positions.size()]);
            posX = (Double[]) runsim.posX.toArray(new Double[runsim.posX.size()]);
            posY = (Double[]) runsim.posY.toArray(new Double[runsim.posY.size()]);

            // Create empty collection xy-chart series
            Collection<XYChart.Data<Double,Double>> sigmaXs = new ArrayList<>(runsim.positions.size());
            Collection<XYChart.Data<Double,Double>> sigmaYs = new ArrayList<>(runsim.positions.size());
            Collection<XYChart.Data<Double,Double>> positionss = new ArrayList<>(runsim.positions.size());
            Collection<XYChart.Data<Double,Double>> posXs = new ArrayList<>(runsim.positions.size());
            Collection<XYChart.Data<Double,Double>> posYs = new ArrayList<>(runsim.positions.size());

            // Fill up the collection xy-chart series
            for (int i = 0; i < runsim.positions.size(); i++){
                XYChart.Data sigmaXn = new XYChart.Data<Double, Double>(positions[i], sigmaX[i]); sigmaXs.add(sigmaXn);
                XYChart.Data sigmaYn = new XYChart.Data<Double, Double>(positions[i], sigmaY[i]); sigmaYs.add(sigmaYn);
                XYChart.Data posXn = new XYChart.Data<Double, Double>(positions[i], posX[i]); posXs.add(posXn);
                XYChart.Data posYn = new XYChart.Data<Double, Double>(positions[i], posY[i]); posYs.add(posYn);
            }

            // Fill up the xy-chart series
            seriesSigmaX.getData().addAll(sigmaXs);
            seriesSigmaY.getData().addAll(sigmaYs);
            seriesPositions.getData().addAll(positionss);
            seriesX.getData().addAll(posXs);
            seriesY.getData().addAll(posYs);

            // Set names of the xy-chart series
            seriesSigmaX.setName("RMS X");
            seriesSigmaY.setName("RMS Y");
            seriesX.setName("Pos. X");
            seriesY.setName("Pos. Y");

            // clear previous data from plots
            plot1.getData().clear(); // Centroid / trajectory
            plot2.getData().clear(); // Envelope / RMS
            
            // Plot New Data
            plot1.getData().add(seriesX);
            plot1.getData().add(seriesY);
            plot2.getData().add(seriesSigmaX);
            plot2.getData().add(seriesSigmaY);
            
            // Rotatio
            plot1.getYAxis().setTickLabelRotation(270);
            plot2.getYAxis().setTickLabelRotation(270);
            
            plot1.getStylesheets().add(this.getClass().getResource("/styles/Styles_traj.css").toExternalForm());

            // For the 3D, add sigma Z and show its checkbox:
            if (beamtype == "Bunched"){
                checkboxZ.setDisable(false);
                checkboxZ.setSelected(true);
                Collection<XYChart.Data<Double,Double>> sigmaZs = new ArrayList<>(runsim.positions.size());
                for (int i = 0; i < runsim.positions.size(); i++){ XYChart.Data sigmaZn = new XYChart.Data<Double, Double>(positions[i], sigmaZ[i]); sigmaZs.add(sigmaZn); }
                seriesSigmaZ.getData().addAll(sigmaZs);
                seriesSigmaZ.setName("RMS Z");
                plot2.getData().add(seriesSigmaZ);
                plot2.getStylesheets().add(this.getClass().getResource("/styles/Styles_env3D.css").toExternalForm());
            }
            else{checkboxZ.setDisable(true); checkboxZ.setSelected(false); plot2.getStylesheets().add(this.getClass().getResource("/styles/Styles_env2D.css").toExternalForm());}
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            label.setText("Simulation finished at " + dateFormat.format(date)+".");
            System.out.println("Simulation finished.");
            DateFormat dateFormat2 = new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
            lastsimtext = "simulationdata_" + dateFormat2.format(date);
            checkboxX.setSelected(true);
            checkboxY.setSelected(true);
            checkboxBPMs.setSelected(true);
            checkboxVacuumCh.setSelected(true);
            for (int i = 0; i < runsim.chamberX[0].length ; i++) {
                seriesXchamber.getData().add(new XYChart.Data(runsim.chamberX[0][i], runsim.chamberX[1][i]*1e3));
                seriesYchamber.getData().add(new XYChart.Data(runsim.chamberY[0][i], runsim.chamberY[1][i]*1e3));
            }
            System.out.println(seriesXchamber.getData().toString());
            seriesXchamber.setName("X-aperture");
            seriesYchamber.setName("Y-aperture");
            plot2.getData().add(seriesXchamber);
            plot2.getData().add(seriesYchamber);
            

            Node chartPlotArea = plot1.lookup(".chart-plot-background");
            double chartZeroX = chartPlotArea.getLayoutX();
            // double chartZeroY = chartPlotArea.getLayoutY();
            //System.out.println("Pl x0: "+chartZeroX);
            //System.out.println("Pl y0: "+chartZeroY);
            
            
            
            
            //List<NPM> npms = new ArrayList<>();
            //List<EMU> emus = new ArrayList<>();
            
            
        }
        else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Simulation Error!");
            alert.setHeaderText("No accelerator sequence selected.");
            alert.setContentText("Select an accelerator sequence and try again.");
            alert.showAndWait();
        }
    }
    
    @Override public void initialize(URL url, ResourceBundle rb) {
    }
}
