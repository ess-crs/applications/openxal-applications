package xal.app.virtualmachine;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import xal.tools.beam.PhaseVector;
import xal.tools.beam.Twiss;
import xal.sim.scenario.ProbeFactory;
import xal.sim.scenario.AlgorithmFactory;
import xal.tools.beam.CovarianceMatrix;
import xal.sim.scenario.Scenario;
import xal.model.ModelException;
import xal.model.alg.EnvTrackerAdapt;
import xal.model.probe.EnvelopeProbe;
import xal.model.probe.traj.EnvelopeProbeState;
import xal.model.probe.traj.Trajectory;
import xal.sim.sync.SynchronizationException;
import xal.smf.Accelerator;
import xal.smf.AcceleratorNode;
import xal.smf.AcceleratorSeq;
/**
 *
 * @author benjaminbolling
 */
public class RunSim {
    public VMDocument document;
    protected double beamcurrmaxlim = 90;
    private AcceleratorSeq sequence;
    private EnvTrackerAdapt envelopeTracker;
    private EnvelopeProbe probe;
    private PhaseVector initial_pos;
    private Scenario model;
    private String model_sync;
    protected String beamtype;
    protected ArrayList sigmaX;
    protected ArrayList sigmaY;
    protected ArrayList sigmaZ;
    protected double[][] chamberX;
    protected double[][] chamberY;
    protected ArrayList positions;
    protected ArrayList posX;
    protected ArrayList posY;
    protected double runflag;
    protected ArrayList MagnetNodes;
    protected ArrayList MagnetPositions;
    protected ArrayList MagnetLengths;
    protected ArrayList MagnetTypes;
    protected ArrayList MagnetFields;
    
    public void RunSim(){
        try { String sequencestr = MainFunctions.mainDocument.getSequence().toString(); runflag = 1;}
        catch(NullPointerException e) { runflag = 0;  return; }
        // Load all the stuff from the VM document
        double beamcurrent = MainFunctions.mainDocument.getCurrent();
        double doubleTotE = MainFunctions.mainDocument.getTotEnergy();
        beamtype = MainFunctions.mainDocument.getProbeType().get();
        model_sync = MainFunctions.mainDocument.getModel().get();
        
        double alphaX = MainFunctions.mainDocument.getALPHAX();
        double alphaY = MainFunctions.mainDocument.getALPHAY();
        double alphaZ = MainFunctions.mainDocument.getALPHAZ();
        
        double betaX = MainFunctions.mainDocument.getBETAX();
        double betaY = MainFunctions.mainDocument.getBETAY();
        double betaZ = MainFunctions.mainDocument.getBETAZ();
        
        double emittX = MainFunctions.mainDocument.getEMITTX();
        double emittY = MainFunctions.mainDocument.getEMITTY();
        double emittZ = MainFunctions.mainDocument.getEMITTZ();
        
        double x0 = MainFunctions.mainDocument.getX()*1e-03;
        double y0 = MainFunctions.mainDocument.getY()*1e-03;
        double z0 = MainFunctions.mainDocument.getZ()*1e-03;
        
        double x0p = MainFunctions.mainDocument.getXP()*1e-03;
        double y0p = MainFunctions.mainDocument.getYP()*1e-03;
        double z0p = MainFunctions.mainDocument.getZP()*1e-03;
        
        double SPACE_CHARGE = MainFunctions.mainDocument.getSCC();
        
//        double stepsize = MainFunctions.mainDocument.getStepSize();
        
        String sequencestr = MainFunctions.mainDocument.getSequence().toString();
        Accelerator accelerator = MainFunctions.mainDocument.getAccelerator();
        
        initial_pos = PhaseVector.newZero();
        String inipos = "" + x0 + "," + x0p + "," + y0 + "," + y0p + "," + z0 + "," + z0p;
        initial_pos = PhaseVector.parse(inipos);
        System.out.println("Running simulation for sequence: "+MainFunctions.mainDocument.getSequence().toString());
        if(accelerator.getComboSequences().toString().contains(sequencestr)){ sequence = accelerator.getComboSequence(sequencestr); }   else    { sequence = accelerator.getSequence(sequencestr); }
        // Get all accelerator nodes
        List<AcceleratorNode> accNodes = sequence.getNodes();
        System.out.println("Nodes: "+accNodes);
        // Get only the nodes that are magnets and some properties
        MagnetNodes = new ArrayList<String>();
        MagnetPositions = new ArrayList<Double>();
        MagnetLengths = new ArrayList<Double>();
        MagnetTypes = new ArrayList<String>();
        MagnetFields = new ArrayList<String>();
        for (int counter = 0; counter < accNodes.size(); counter++) { Object accNode = accNodes.get(counter); Object accNodeObj = accelerator.getNode(accNode.toString());
            if (accelerator.getNode(accNode.toString()).isMagnet()){ MagnetNodes.add(accNodeObj.toString()); MagnetLengths.add(accelerator.getNode(accNode.toString()).getLength()); MagnetTypes.add(accelerator.getNode(accNode.toString()).getType()); MagnetPositions.add(accelerator.getNode(accNode.toString()).getPosition() - accelerator.getNode(accNode.toString()).getLength()/2); 
            // How do we read the field value ?? ... >> . <<
            //MagnetFields.add(accelerator.getNode(accNode.toString()).getChannel("fieldRB").toString());
            } }
        try { envelopeTracker = AlgorithmFactory.createEnvTrackerAdapt((AcceleratorSeq) sequence); } catch (InstantiationException ex) { Logger.getLogger(RunSim.class.getName()).log(Level.SEVERE, null, ex); }
        envelopeTracker.setUseSpacecharge(true);
        envelopeTracker.setStepSize(0.001);
        probe = ProbeFactory.getEnvelopeProbe((AcceleratorSeq) sequence, envelopeTracker);
        double kinE = probe.getKineticEnergy();
        System.out.println("Kin E: "+kinE);
        probe.setKineticEnergy(doubleTotE);
        probe.setCurrentElement(((AcceleratorSeq) sequence).getNodeAt(0).toString());
        double beta_gamma = probe.getGamma()*probe.getBeta();
        Twiss twissX = new Twiss(alphaX,betaX,emittX/beta_gamma);
        Twiss twissY = new Twiss(alphaY,betaY,emittY/beta_gamma);
        Twiss twissZ = new Twiss(alphaZ,betaZ,emittZ/beta_gamma);
        CovarianceMatrix cov = CovarianceMatrix.buildCovariance(twissX,twissY,twissZ,initial_pos);
        if (beamtype == "Bunched"){ envelopeTracker.setUseDCBeam(false); probe.setBeamCurrent(SPACE_CHARGE*beamcurrent*35/1000); }
        if (beamtype == "DC"){ envelopeTracker.setUseDCBeam(true); probe.setBeamCurrent(SPACE_CHARGE*beamcurrent/1000); }
        probe.setCovariance(cov);
        probe.setPosition(0.0);
        try { model = Scenario.newScenarioFor((AcceleratorSeq) sequence); } catch (ModelException ex) { Logger.getLogger(RunSim.class.getName()).log(Level.SEVERE, null, ex); }
        model.setProbe(probe);
        model.setSynchronizationMode(model_sync);
        // Re-synchronize and run the model:
        try { model.resync(); model.run(); } catch (SynchronizationException ex) { Logger.getLogger(RunSim.class.getName()).log(Level.SEVERE, null, ex); } catch (ModelException ex) { Logger.getLogger(RunSim.class.getName()).log(Level.SEVERE, null, ex); }
        EnvelopeProbe probe_final = (EnvelopeProbe) model.getProbe();
        Trajectory trajectory = probe_final.getTrajectory();
        ArrayList<EnvelopeProbeState> stateElement = (ArrayList<EnvelopeProbeState>) trajectory.getStatesViaIndexer();
        CovarianceMatrix covmat;
        positions = new ArrayList<Double>();
        posX = new ArrayList<Double>();
        posY = new ArrayList<Double>();
        sigmaX = new ArrayList<Double>();
        sigmaY = new ArrayList<Double>();
        sigmaZ = new ArrayList<Double>();
        chamberX = sequence.getAperProfile().getProfileXArray();
        chamberY = sequence.getAperProfile().getProfileYArray();
        for(int i=0; i<trajectory.numStates(); i++){
            stateElement.get(i).getPosition();
            positions.add(i,stateElement.get(i).getPosition());
            covmat = stateElement.get(i).getCovarianceMatrix();
            posX.add(covmat.getMeanX() * 1.0e+3);
            posY.add(covmat.getMeanY() * 1.0e+3);
            sigmaX.add(covmat.getSigmaX() * 1.0e+3);
            sigmaY.add(covmat.getSigmaY() * 1.0e+3);
            if ("Bunched".equals(beamtype)){
                sigmaZ.add(covmat.getSigmaZ() * 1.0e+3);
            }
        }
    }
}
