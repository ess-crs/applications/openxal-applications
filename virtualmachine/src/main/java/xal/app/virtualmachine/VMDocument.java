/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xal.app.virtualmachine;

import java.net.URL;
import java.util.Map;
import javafx.beans.property.SimpleStringProperty;
import javafx.stage.Stage;
import xal.extension.fxapplication.XalFxDocument;

/**
 *
 * @author nataliamilas
 * @author benjaminbolling
 * 
 */
public class VMDocument extends XalFxDocument {
    /**
     * String indicating which model to be used in the simulation
     */
    public SimpleStringProperty model;
    public SimpleStringProperty probeType;
    public double beamcurrent;
    public double X;
    public double XP;
    public double Y;
    public double YP;
    public double Z;
    public double ZP;
    public double ALPHAX;
    public double ALPHAY;
    public double ALPHAZ;
    public double BETAX;
    public double BETAY;
    public double BETAZ;
    public double EMITTX;
    public double EMITTY;
    public double EMITTZ;
    public double totalEnergy;
    public double SCC;

    public VMDocument(Stage stage) { super(stage); model = new SimpleStringProperty(); model.set("DESIGN"); probeType = new SimpleStringProperty(); probeType.set("DC");
        DEFAULT_FILENAME="Lebt.xml"; WILDCARD_FILE_EXTENSION = "*.xml"; HELP_PAGEID="255603173"; } //  Create a new empty LEBT Document

    /**
     *  Create a new document loaded from the URL file
     *
     *@param  url  The URL of the file to load into the new document.
     *@param stage
     */
    public VMDocument(URL url, Stage stage) { this(stage);}

    /**
     *  Reads the content of the document from the specified URL.
     *
     *@param  url  Description of the Parameter
     */
    public final void readScanDocument(URL url) { }
    /**
     *  Save the ScannerDocument document to the specified URL.
     *
     *  @param  url  The file URL where the data should be saved
     */
    @Override
    public void saveDocumentAs(URL url) { }

    /**
     *  Reads the content of the document from the specified URL, and loads the information into the application.
     *
     * @param  url  The path to the XML file
     */
    public void loadDocument(URL url) { }

    public SimpleStringProperty getModel() { return model; }                            // Get the current model
    public SimpleStringProperty getProbeType() { return probeType; }                    // Get the current probe type
    public double getCurrent() { return beamcurrent; }                                  // Get the current current
    public double getTotEnergy() { return totalEnergy; }                                // Get the current total energy
    public double getSCC() { return SCC; }                                              // Get the current total energy
    
    public void setModel(String model) { this.model.set(model); }                       // Set the current model ; @param model string indication to use with Live or Design models
    public void setProbeType(String probeType) { this.probeType.set(probeType); }       // Set the probe type
    public void setCurrent(double current) { this.beamcurrent = current; }              // Set the current current
    public void setTotEnergy(double totalEnergy) { this.totalEnergy = totalEnergy; }    //  Set the current total energy
    public void setSCC(double SCC) { this.SCC = SCC; }    //  Set the current total energy

    public double getX() { return X; }                                                  // Get the current X position
    public double getY() { return Y; }                                                  // Get the current Y position
    public double getZ() { return Z; }                                                  // Get the current Z position
    
    public void setX(double X) { this.X = X; }                                          // Set the X position
    public void setY(double Y) { this.Y = Y; }                                          // Set the Y position
    public void setZ(double Z) { this.Z = Z; }                                          // Set the Z position
    
    public double getXP() { return XP; }                                                // Get the current XP value
    public double getYP() { return YP; }                                                // Get the current YP value
    public double getZP() { return ZP; }                                                // Get the current ZP value
    
    public void setXP(double XP) { this.XP = XP; }                                      // Set the XP value
    public void setYP(double YP) { this.YP = YP; }                                      // Set the YP value
    public void setZP(double ZP) { this.ZP = ZP; }                                      // Set the ZP value
    
    public double getALPHAX() { return ALPHAX; }                                        // Get the current ALPHAX parameter
    public double getALPHAY() { return ALPHAY; }                                        // Get the current ALPHAY parameter
    public double getALPHAZ() { return ALPHAZ; }                                        // Get the current ALPHAZ parameter
    
    public void setALPHAX(double ALPHAX) { this.ALPHAX = ALPHAX; }                      // Set the ALPHAX parameter
    public void setALPHAY(double ALPHAY) { this.ALPHAY = ALPHAY; }                      // Set the ALPHAY parameter
    public void setALPHAZ(double ALPHAZ) { this.ALPHAZ = ALPHAZ; }                      // Set the ALPHAZ parameter
    
    public double getBETAX() { return BETAX; }                                          // Get the current BETAX parameter
    public double getBETAY() { return BETAY; }                                          // Get the current BETAY parameter
    public double getBETAZ() { return BETAZ; }                                          // Get the current BETAZ parameter
    
    public void setBETAX(double BETAX) { this.BETAX = BETAX; }                          // Set the BETAX parameter
    public void setBETAY(double BETAY) { this.BETAY = BETAY; }                          // Set the BETAY parameter
    public void setBETAZ(double BETAZ) { this.BETAZ = BETAZ; }                          // Set the BETAZ parameter
    
    public double getEMITTX() { return EMITTX; }                                        //  Get the current EMITT(ance)X parameter
    public double getEMITTY() { return EMITTY; }                                        //  Get the current EMITT(ance)Y parameter
    public double getEMITTZ() { return EMITTZ; }                                        //  Get the current EMITT(ance)Z parameter
    
    public void setEMITTX(double EMITTX) { this.EMITTX = EMITTX; }                      //  Set the EMITT(ance)X parameter
    public void setEMITTY(double EMITTY) { this.EMITTY = EMITTY; }                      //  Set the EMITT(ance)Y parameter
    public void setEMITTZ(double EMITTZ) { this.EMITTZ = EMITTZ; }                      //  Set the EMITT(ance)Z parameter
}

