package xal.app.virtualmachine;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Optional;

import static javafx.application.Application.launch;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;

import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.apache.commons.lang3.math.NumberUtils;

import xal.extension.fxapplication.FxApplication;
import xal.extension.fxapplication.XalFxDocument;

public class MainApp extends FxApplication {
    double beamcurrmaxlim = 200;
    double beamcurrent = 74;
    double doubleTotE = 74671.738;
    double doubleSCC = 0.95;
    
    double doubleDx = 1;
    double doubleDxp = 2;
    double doubleDy = -1;
    double doubleDyp = 2;
    double doubleDz = 0;
    double doubleDzp = 0;

    double doubleAlphaX = -3.2288082;
    double doubleAlphaY = -6.2846641;
    double doubleAlphaZ = 0.0;

    double doubleBetaX = 0.38607772;
    double doubleBetaY = 0.39203602;
    double doubleBetaZ = 1.0;

    double doubleEmittX = 0.1223e-06;
    double doubleEmittY = 0.1217e-06;
    double doubleEmittZ = 1.0;
    
    @Override
    public void start(Stage stage) throws IOException {
        MAIN_SCENE = "/fxml/VMScene.fxml";
        CSS_STYLE = "/styles/Styles.css";
        setApplicationName("Virtual Machine");
        HAS_DOCUMENTS= true;
        HAS_SEQUENCE = true;
        DOCUMENT = new VMDocument(stage);
        MainFunctions.initialize((VMDocument)DOCUMENT);
        super.initialize();
        
        // New roll-down menu   
        Menu modelMenu = new Menu("Model");
        ToggleGroup modelGroup = new ToggleGroup();
        
        // New radiomenuitem : DESIGN
        RadioMenuItem modelDesignMenu = new RadioMenuItem("DESIGN");
        modelDesignMenu.setOnAction(new ModelMenu((VMDocument) DOCUMENT, modelGroup));
        modelMenu.getItems().add(modelDesignMenu );
        modelGroup.getToggles().add(modelDesignMenu);
        
        // New radiomenuitem : LIVE
        RadioMenuItem modelLiveMenu =new RadioMenuItem("LIVE");
        modelLiveMenu.setOnAction(new ModelMenu((VMDocument) DOCUMENT, modelGroup));
        modelMenu.getItems().add(modelLiveMenu);
        modelGroup.getToggles().add(modelLiveMenu);
        
        // Add roll-down menu to menubar
        modelGroup.selectToggle(modelDesignMenu);
        MENU_BAR.getMenus().add(MENU_BAR.getMenus().size()-2, modelMenu);
        
        // New roll-down menu
        Menu probeMenu = new Menu("Probe Setup");
        ToggleGroup probeGroup = new ToggleGroup();
        
        // Create the menu items and submenus
        MenuItem chinitcondMenuItem = new MenuItem("Change init. cond.");
        MenuItem chcurrMenuItem = new MenuItem("Change current");
        Menu chbeamMenuItem = new Menu("Change beam");
        RadioMenuItem beamDCMenuItem = new RadioMenuItem("DC");
        RadioMenuItem beamBunchedMenuItem = new RadioMenuItem("Bunched");
        
        // Layout the items in the 'Probe Setup' menu
        probeMenu.getItems().add(chinitcondMenuItem);
        probeMenu.getItems().add(chcurrMenuItem);
        probeMenu.getItems().add(chbeamMenuItem);
        
        // Layout items in the 'Change beam' submenu
        chbeamMenuItem.getItems().add(beamDCMenuItem);
        chbeamMenuItem.getItems().add(beamBunchedMenuItem);
        
        // Make one and only one item toggled in the 'Change beam' submenu
        probeGroup.getToggles().add(beamDCMenuItem);
        probeGroup.getToggles().add(beamBunchedMenuItem);
        probeGroup.selectToggle(beamDCMenuItem); // DC beam is automatically selected when launching
        
        beamDCMenuItem.setOnAction(new BeamProbeMenu((VMDocument) DOCUMENT, probeGroup));
        beamBunchedMenuItem.setOnAction(new BeamProbeMenu((VMDocument) DOCUMENT, probeGroup));
        setInitParams setParams = new setInitParams();
        // Connect chinitcondMenuItem with and create its function
        EventHandler<ActionEvent> chinitcondMenuItemevent = new EventHandler<ActionEvent>() { 
            public void handle(ActionEvent e) { 
                Dialog initconddialog = new Dialog();
                initconddialog.setTitle("Set beam conditions");
                
                if(modelDesignMenu.isSelected()){ initconddialog.setHeaderText("Model: DESIGN"); }
                else{ if(modelLiveMenu.isSelected()){ initconddialog.setHeaderText("Model: LIVE"); } 
                    else{ initconddialog.setHeaderText("Model: Unknown"); } } // Just in case ...
                
                DialogPane maincondsdialogPane = initconddialog.getDialogPane();
                GridPane gridPane = new GridPane();
                gridPane.setHgap(10);
                gridPane.setVgap(10);
                
                // Construct the initial conditions' and TWISS parameters' editable TextFields.
                TextField textFieldDx = new TextField( String.valueOf (doubleDx) );
                TextField textFieldDxp = new TextField(String.valueOf (doubleDxp) );
                TextField textFieldDy = new TextField(String.valueOf (doubleDy) );
                TextField textFieldDyp = new TextField(String.valueOf (doubleDyp) );
                TextField textFieldDz = new TextField(String.valueOf (doubleDz) );
                TextField textFieldDzp = new TextField(String.valueOf (doubleDzp) );
                TextField textFieldAlphaX = new TextField(String.valueOf (doubleAlphaX));
                TextField textFieldAlphaY = new TextField(String.valueOf (doubleAlphaY));
                TextField textFieldAlphaZ = new TextField(String.valueOf (doubleAlphaZ));
                TextField textFieldBetaX = new TextField(String.valueOf (doubleBetaX));
                TextField textFieldBetaY = new TextField(String.valueOf (doubleBetaY));
                TextField textFieldBetaZ = new TextField(String.valueOf (doubleBetaZ));
                TextField textFieldEmittX = new TextField(String.valueOf (doubleEmittX));
                TextField textFieldEmittY = new TextField(String.valueOf (doubleEmittY));
                TextField textFieldEmittZ = new TextField(String.valueOf (doubleEmittZ));
                TextField textFieldTotE = new TextField(String.valueOf (doubleTotE));
                TextField textFieldSCC = new TextField(String.valueOf (doubleSCC));

                // Construct column 1: Initial conditions
                gridPane.add(new Label("Initial conditions:"), 2, 0);
                gridPane.add(new Label("D_x:"), 1, 1); // X
                GridPane.setConstraints(textFieldDx, 2, 1); // X
                gridPane.add(new Label(" mm."), 3, 1); // X
                gridPane.add(new Label("D_x':"), 1, 2); // X'
                GridPane.setConstraints(textFieldDxp, 2, 2); // X'
                gridPane.add(new Label(" mrad."), 3, 2); // X'
                gridPane.add(new Label("D_y:"), 1, 3); // Y
                GridPane.setConstraints(textFieldDy, 2, 3); // Y
                gridPane.add(new Label(" mm."), 3, 3); // Y
                gridPane.add(new Label("D_y':"), 1, 4); // Y'
                GridPane.setConstraints(textFieldDyp, 2, 4); // Y'
                gridPane.add(new Label(" mrad."), 3, 4); // Y'
                gridPane.add(new Label("D_z:"), 1, 5); // Z
                GridPane.setConstraints(textFieldDz, 2, 5); // Z
                gridPane.add(new Label(" mm."), 3, 5); // Z
                gridPane.add(new Label("D_z':"), 1, 6); // Z'
                GridPane.setConstraints(textFieldDzp, 2, 6); // Z'
                gridPane.add(new Label(" mrad."), 3, 6); // Z'
                gridPane.add(new Label("----------"), 1, 7); // ---
                gridPane.add(new Label("----------"), 2, 7); // ---
                gridPane.add(new Label("---"), 3, 7); // ---
                gridPane.add(new Label("Tot. E:"), 1, 8); // tot. E
                GridPane.setConstraints(textFieldTotE, 2, 8); // tot. E
                gridPane.add(new Label(" eV."), 3, 8); // tot. E
                gridPane.add(new Label("Space-charge:"), 1, 9); // SCC
                GridPane.setConstraints(textFieldSCC, 2, 9); // SCC
                gridPane.add(new Label("."), 3, 9); // SCC
                
                // Construct column 2: TWISS
                gridPane.add(new Label("TWISS parameters:"), 5, 0);
                gridPane.add(new Label("\u03B1_x:"), 4, 1);
                GridPane.setConstraints(textFieldAlphaX, 5, 1);
                gridPane.add(new Label("\u03B1_y:"), 4, 2);
                GridPane.setConstraints(textFieldAlphaY, 5, 2);
                gridPane.add(new Label("\u03B1_z:"), 4, 3);
                GridPane.setConstraints(textFieldAlphaZ, 5, 3);
                gridPane.add(new Label("\u03B2_x:"), 4, 4);
                GridPane.setConstraints(textFieldBetaX, 5, 4);
                gridPane.add(new Label("\u03B2_y:"), 4, 5);
                GridPane.setConstraints(textFieldBetaY, 5, 5);
                gridPane.add(new Label("\u03B2_z:"), 4, 6);
                GridPane.setConstraints(textFieldBetaZ, 5, 6);
                gridPane.add(new Label("\u03B5_x:"), 4, 7);
                GridPane.setConstraints(textFieldEmittX, 5, 7);
                gridPane.add(new Label("\u03B5_y:"), 4, 8);
                GridPane.setConstraints(textFieldEmittY, 5, 8);
                gridPane.add(new Label("\u03B5_z:"), 4, 9);
                GridPane.setConstraints(textFieldEmittZ, 5, 9);
                
                // Build it
                gridPane.getChildren().addAll(textFieldDx, textFieldDxp, textFieldDy, textFieldDyp, textFieldDz, textFieldDzp, textFieldTotE, textFieldSCC);
                gridPane.getChildren().addAll(textFieldAlphaX, textFieldAlphaY, textFieldAlphaZ, textFieldBetaX, textFieldBetaY, textFieldBetaZ, textFieldEmittX, textFieldEmittY, textFieldEmittZ);
                maincondsdialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
                initconddialog.getDialogPane().setContent(gridPane);
                
                // Run it
                Optional<ButtonType> result = initconddialog.showAndWait(); // Show and wait for results (which btn is clicked: Ok or cancel?)
                if(result.get() == ButtonType.OK){
                    // Get the new initial conditions and TWISS parameter values
                    String doubleDxnew = new String(textFieldDx.getText());
                    String doubleDxpnew = new String(textFieldDxp.getText());
                    String doubleDynew = new String(textFieldDy.getText());
                    String doubleDypnew = new String(textFieldDyp.getText());
                    String doubleDznew = new String(textFieldDz.getText());
                    String doubleDzpnew = new String(textFieldDzp.getText());
                    String doubleTotEnew = new String(textFieldTotE.getText());
                    String doubleSCCnew = new String(textFieldSCC.getText());
                    String doubleAlphaXnew = new String(textFieldAlphaX.getText());
                    String doubleAlphaYnew = new String(textFieldAlphaY.getText());
                    String doubleAlphaZnew = new String(textFieldAlphaZ.getText());
                    String doubleBetaXnew = new String(textFieldBetaX.getText());
                    String doubleBetaYnew = new String(textFieldBetaY.getText());
                    String doubleBetaZnew = new String(textFieldBetaZ.getText());
                    String doubleEmittXnew = new String(textFieldEmittX.getText());
                    String doubleEmittYnew = new String(textFieldEmittY.getText());
                    String doubleEmittZnew = new String(textFieldEmittZ.getText());
                    
                    // Assign to double only if they are numerical.
                    if (NumberUtils.isNumber(doubleDxnew))  {doubleDx  = Double.valueOf(doubleDxnew);  textFieldDx.setText(  String.valueOf (doubleDx)  );}
                    if (NumberUtils.isNumber(doubleDynew))  {doubleDy  = Double.valueOf(doubleDynew);  textFieldDy.setText(  String.valueOf (doubleDy)  );}
                    if (NumberUtils.isNumber(doubleDznew))  {doubleDz  = Double.valueOf(doubleDznew);  textFieldDz.setText(  String.valueOf (doubleDz)  );}
                    if (NumberUtils.isNumber(doubleDxpnew)) {doubleDxp = Double.valueOf(doubleDxpnew); textFieldDxp.setText( String.valueOf (doubleDxp) );}
                    if (NumberUtils.isNumber(doubleDypnew)) {doubleDyp = Double.valueOf(doubleDypnew); textFieldDyp.setText( String.valueOf (doubleDyp) );}
                    if (NumberUtils.isNumber(doubleDzpnew)) {doubleDzp = Double.valueOf(doubleDzpnew); textFieldDzp.setText( String.valueOf (doubleDzp) );}
                    
                    if (NumberUtils.isNumber(doubleTotEnew)) {doubleTotE = Double.valueOf(doubleTotEnew); textFieldTotE.setText( String.valueOf (doubleTotE) );}
                    if (NumberUtils.isNumber(doubleSCCnew)) {doubleSCC = Double.valueOf(doubleSCCnew); textFieldSCC.setText( String.valueOf (doubleSCC) );}
                    
                    if (NumberUtils.isNumber(doubleAlphaXnew)) {doubleAlphaX = Double.valueOf(doubleAlphaXnew); textFieldAlphaX.setText( String.valueOf (doubleAlphaX) );}
                    if (NumberUtils.isNumber(doubleAlphaYnew)) {doubleAlphaY = Double.valueOf(doubleAlphaYnew); textFieldAlphaY.setText( String.valueOf (doubleAlphaY) );}
                    if (NumberUtils.isNumber(doubleAlphaZnew)) {doubleAlphaZ = Double.valueOf(doubleAlphaZnew); textFieldAlphaZ.setText( String.valueOf (doubleAlphaZ) );}
                    
                    if (NumberUtils.isNumber(doubleBetaXnew)) {doubleBetaX = Double.valueOf(doubleBetaXnew); textFieldBetaX.setText( String.valueOf (doubleBetaX) );}
                    if (NumberUtils.isNumber(doubleBetaYnew)) {doubleBetaY = Double.valueOf(doubleBetaYnew); textFieldBetaY.setText( String.valueOf (doubleBetaY) );}
                    if (NumberUtils.isNumber(doubleBetaZnew)) {doubleBetaZ = Double.valueOf(doubleBetaZnew); textFieldBetaZ.setText( String.valueOf (doubleBetaZ) );}
                    
                    if (NumberUtils.isNumber(doubleEmittXnew)) {doubleEmittX = Double.valueOf(doubleEmittXnew); textFieldEmittX.setText( String.valueOf (doubleEmittX) );}
                    if (NumberUtils.isNumber(doubleEmittYnew)) {doubleEmittY = Double.valueOf(doubleEmittYnew); textFieldEmittY.setText( String.valueOf (doubleEmittY) );}
                    if (NumberUtils.isNumber(doubleEmittZnew)) {doubleEmittZ = Double.valueOf(doubleEmittZnew); textFieldEmittZ.setText( String.valueOf (doubleEmittZ) );}
                    
                    // Set the assigned parameters in the VMdocument
                    setParams.setInitParams((VMDocument) DOCUMENT, doubleDx, doubleDxp, doubleDy, doubleDyp, doubleDz, doubleDzp, doubleAlphaX, doubleAlphaY, doubleAlphaZ, doubleBetaX, doubleBetaY, doubleBetaZ, doubleEmittX, doubleEmittY, doubleEmittZ, doubleTotE, doubleSCC, beamcurrent);
                }
            } 
        };
        chinitcondMenuItem.setOnAction(chinitcondMenuItemevent);
        setParams.setInitParams((VMDocument) DOCUMENT, doubleDx, doubleDxp, doubleDy, doubleDyp, doubleDz, doubleDzp, doubleAlphaX, doubleAlphaY, doubleAlphaZ, doubleBetaX, doubleBetaY, doubleBetaZ, doubleEmittX, doubleEmittY, doubleEmittZ, doubleTotE, doubleSCC, beamcurrent);
        // Connect chcurrMenuItem with and create its function
        EventHandler<ActionEvent> chcurrMenuItemevent = new EventHandler<ActionEvent>() { 
            public VMDocument document;
            public void handle(ActionEvent e) {
                TextInputDialog currdialog = new TextInputDialog(String.valueOf(beamcurrent));
                currdialog.setTitle("Set beam current");
                currdialog.setContentText("Set beam current [mA]:");
                Optional<String> result = currdialog.showAndWait(); // Get the response value.
                if (result.isPresent()){ // check if a new current has been assigned (is Ok pressed or not)
                    if (!result.get().matches("\\d{0,5}([\\.]\\d{0,4})?")) { System.out.println("Input value error: " + result.get() + " is not a valid numerical input: Input must be numerical, cannot be negative, and has to consist of maximum 6 digits. Beam current remains at " + beamcurrent + " mA.");} // Input was not numeric.
                    else {
                        if (Double.valueOf(result.get()) > beamcurrmaxlim) { System.out.println("Input value error: " + result.get() + " must be smaller than " + String.valueOf(beamcurrmaxlim) + " mA. Beam current remains at " + beamcurrent + " mA.");} // Input beam current was too high. Real machine: 62.5 mA.
                        else { beamcurrent = Double.valueOf(result.get()); setBeamCurrent setBeam = new setBeamCurrent(); setBeam.setBeamCurrent((VMDocument) DOCUMENT, beamcurrent); } // All seems to be in order captain, the new current has been accepted.
                    }
                }
            }
        };
        chcurrMenuItem.setOnAction(chcurrMenuItemevent);
        // Add the 'Probe Setup' menu to the window's menubar
        MENU_BAR.getMenus().add(MENU_BAR.getMenus().size()-2, probeMenu);
        super.start(stage);
    }
    
    public VMDocument retDOC(){
        //return (VMDocument) DOCUMENT;
        return (VMDocument) this.DOCUMENT;
    }
    
    public XalFxDocument retFxDOC(){
        return this.DOCUMENT;
    }
    
    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) { launch(args); } }

class setBeamCurrent{ protected VMDocument document; protected double beamcurrent;
    public void setBeamCurrent(VMDocument document, double beamcurrent){
        System.out.println("Previous beam current from VMdoc: " + String.valueOf(document.getCurrent()) + " mA.");
        document.setCurrent(beamcurrent);
        System.out.println("New beam current is: " + String.valueOf(document.getCurrent()) + " mA.");
    }
}

class setInitParams extends ActionEvent {
    public VMDocument document;
    protected double X;
    protected double XP;
    protected double Y;
    protected double YP;
    protected double Z;
    protected double ZP;
    protected double ALPHAX;
    protected double ALPHAY;
    protected double ALPHAZ;
    protected double BETAX;
    protected double BETAY;
    protected double BETAZ;
    protected double EMITTX;
    protected double EMITTY;
    protected double EMITTZ;
    protected double doubleTotE;
    protected double doubleSCC;
    protected double beamcurrent;
    
    public void setInitParams(VMDocument document, double doubleDx, double doubleDxp, double doubleDy, double doubleDyp, double doubleDz, double doubleDzp, double doubleAlphaX, double doubleAlphaY, double doubleAlphaZ, double doubleBetaX, double doubleBetaY, double doubleBetaZ, double doubleEmittX, double doubleEmittY, double doubleEmittZ, double doubleTotE, double doubleSCC, double beamcurrent){
        System.out.println("Previous X from VMdoc: " + String.valueOf(document.getX()) + ".");
        System.out.println("Previous Y from VMdoc: " + String.valueOf(document.getY()) + ".");
        System.out.println("Previous Z from VMdoc: " + String.valueOf(document.getZ()) + ".");
        System.out.println("Previous XP from VMdoc: " + String.valueOf(document.getXP()) + ".");
        System.out.println("Previous YP from VMdoc: " + String.valueOf(document.getYP()) + ".");
        System.out.println("Previous ZP from VMdoc: " + String.valueOf(document.getZP()) + ".");
        System.out.println("Previous ALPHAX from VMdoc: " + String.valueOf(document.getALPHAX()) + ".");
        System.out.println("Previous ALPHAY from VMdoc: " + String.valueOf(document.getALPHAY()) + ".");
        System.out.println("Previous ALPHAZ from VMdoc: " + String.valueOf(document.getALPHAZ()) + ".");
        System.out.println("Previous BETAX from VMdoc: " + String.valueOf(document.getBETAX()) + ".");
        System.out.println("Previous BETAY from VMdoc: " + String.valueOf(document.getBETAY()) + ".");
        System.out.println("Previous BETAZ from VMdoc: " + String.valueOf(document.getBETAZ()) + ".");
        System.out.println("Previous EMITTX from VMdoc: " + String.valueOf(document.getEMITTX()) + ".");
        System.out.println("Previous EMITTY from VMdoc: " + String.valueOf(document.getEMITTY()) + ".");
        System.out.println("Previous EMITTZ from VMdoc: " + String.valueOf(document.getEMITTZ()) + ".");
        System.out.println("Previous TotEnergy from VMdoc: " + String.valueOf(document.getTotEnergy()) + ".");
        System.out.println("Previous Space-Charge from VMdoc: " + String.valueOf(document.getSCC()) + ".");
        System.out.println("Previous BeamCurrent from VMdoc: " + String.valueOf(document.getCurrent()) + ".");
        document.setX(doubleDx);
        document.setY(doubleDy);
        document.setZ(doubleDz);
        document.setXP(doubleDxp);
        document.setYP(doubleDyp);
        document.setZP(doubleDzp);
        document.setALPHAX(doubleAlphaX);
        document.setALPHAY(doubleAlphaY);
        document.setALPHAZ(doubleAlphaZ);
        document.setBETAX(doubleBetaX);
        document.setBETAY(doubleBetaY);
        document.setBETAZ(doubleBetaZ);
        document.setEMITTX(doubleEmittX);
        document.setEMITTY(doubleEmittY);
        document.setEMITTZ(doubleEmittZ);
        document.setTotEnergy(doubleTotE);
        document.setSCC(doubleSCC);
        document.setCurrent(beamcurrent);
        System.out.println("New X from VMdoc: " + String.valueOf(document.getX()) + ".");
        System.out.println("New Y from VMdoc: " + String.valueOf(document.getY()) + ".");
        System.out.println("New Z from VMdoc: " + String.valueOf(document.getZ()) + ".");
        System.out.println("New XP from VMdoc: " + String.valueOf(document.getXP()) + ".");
        System.out.println("New YP from VMdoc: " + String.valueOf(document.getYP()) + ".");
        System.out.println("New ZP from VMdoc: " + String.valueOf(document.getZP()) + ".");
        System.out.println("New ALPHAX from VMdoc: " + String.valueOf(document.getALPHAX()) + ".");
        System.out.println("New ALPHAY from VMdoc: " + String.valueOf(document.getALPHAY()) + ".");
        System.out.println("New ALPHAZ from VMdoc: " + String.valueOf(document.getALPHAZ()) + ".");
        System.out.println("New BETAX from VMdoc: " + String.valueOf(document.getBETAX()) + ".");
        System.out.println("New BETAY from VMdoc: " + String.valueOf(document.getBETAY()) + ".");
        System.out.println("New BETAZ from VMdoc: " + String.valueOf(document.getBETAZ()) + ".");
        System.out.println("New EMITTX from VMdoc: " + String.valueOf(document.getEMITTX()) + ".");
        System.out.println("New EMITTY from VMdoc: " + String.valueOf(document.getEMITTY()) + ".");
        System.out.println("New EMITTZ from VMdoc: " + String.valueOf(document.getEMITTZ()) + ".");
        System.out.println("New TotEnergy from VMdoc: " + String.valueOf(document.getTotEnergy()) + ".");
        System.out.println("New space-charge from VMdoc: " + String.valueOf(document.getSCC()) + ".");
        System.out.println("New BeamCurrent from VMdoc: " + String.valueOf(document.getCurrent()) + ".");
    }
}

class ModelMenu implements EventHandler {
    public VMDocument document;
    protected ToggleGroup modelGroup;
    ModelMenu(VMDocument document, ToggleGroup modelGroup){
        this.document = document;
        this.modelGroup = modelGroup;
    }

    @Override
    public void handle(Event t) {
        RadioMenuItem menu = (RadioMenuItem) modelGroup.getSelectedToggle();
        document.setModel(menu.getText());
        Logger.getLogger(VMDocument.class.getName()).log(Level.FINER, "Selected Model {0}",document.getModel().toString());
        System.out.println("Model: " + document.getModel().toString());
    }
}

class BeamProbeMenu implements EventHandler {
    public VMDocument document;
    protected ToggleGroup probeGroup;
    BeamProbeMenu(VMDocument document, ToggleGroup probeGroup){
        this.document = document;
        this.probeGroup = probeGroup;
        // probeGroup
        // probeMenu
    }

    @Override
    public void handle(Event t) {
        RadioMenuItem menu = (RadioMenuItem) probeGroup.getSelectedToggle();
        document.setProbeType(menu.getText());
        Logger.getLogger(VMDocument.class.getName()).log(Level.FINER, "Selected Probe Type {0}",document.getProbeType().toString());
        System.out.println("Probe type: " + document.getProbeType().toString());
    }
}