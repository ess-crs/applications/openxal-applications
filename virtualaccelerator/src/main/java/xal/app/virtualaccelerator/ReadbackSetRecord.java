//
//  ReadbackSetRecord.java
//  xal
//
//  Created by Tom Pelaia on 4/30/2009.
//  Copyright 2009 Oak Ridge National Lab. All rights reserved.
//
package xal.app.virtualaccelerator;

import java.util.*;

import xal.ca.*;
import xal.smf.*;


/** record to cache and synchronize readback and setpoint values */
public class ReadbackSetRecord {
    /** accelerator node */
    final private AcceleratorNode NODE;

    /** readback channel */
    final private Channel READ_CHANNEL;

    /** setpoint channel */
    final private Channel SET_CHANNEL;

    /** conversion factor */
    private double _conversionFactor;


    /** latest setpoint */
    private double _lastSetpoint;

    /** latest readback */
    private double _lastReadback;


    /** Constructor */
    public ReadbackSetRecord( final AcceleratorNode node, final Channel readChannel, final Channel setChannel ) {
        NODE = node;
        READ_CHANNEL = readChannel;
        SET_CHANNEL = setChannel;
    }
    /** Constructor for conversion factor
     *  It is assumed that the first channel holds the real initial configuration
     */
    public ReadbackSetRecord( final AcceleratorNode node, final Channel firstChannel, final Channel secondChannel, final double conversionFactor ) {
        this(node,
                firstChannel,
                secondChannel);
        _conversionFactor = conversionFactor;
        try {
            _lastSetpoint = firstChannel.getValDbl();
            _lastReadback = _lastSetpoint / _conversionFactor;
            secondChannel.putVal(_lastReadback );
        } catch (PutException | GetException e) {
            e.printStackTrace();
        }
    }


    /** get the accelerator node */
    public AcceleratorNode getNode() {
        return NODE;
    }


    /** get the readback channel */
    public Channel getReadbackChannel() {
        return READ_CHANNEL;
    }


    /** get the setpoint channel */
    public Channel getSetpointChannel() {
        return SET_CHANNEL;
    }


    /** get the last setpoint value */
    public double getLastSetpoint() {
        return _lastSetpoint;
    }


    /** get the last readback value */
    public double getLastReadback() {
        return _lastReadback;
    }

    public void setLastSetpoint(Double value){
        try {
            SET_CHANNEL.putVal(value);
        } catch (PutException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    /** update the readback from the setpoint, noise and static error */
    public void updateReadback( final double basisValue, final Map<Channel, Double> noiseMap, final Map<Channel, Double> staticErrorMap, final PutListener listener ) throws Exception {
        if ( READ_CHANNEL != null && SET_CHANNEL != null ) {
            final Double noise = noiseMap.get( READ_CHANNEL );
            final Double staticError = staticErrorMap.get( READ_CHANNEL );
            final double readBack;
            if ( noise != null && staticError != null ) {
                readBack =  NoiseGenerator.setValForPV( basisValue, noise, staticError, true);
            } else {
                readBack =  basisValue;
            }
            _lastSetpoint = basisValue;
            _lastReadback = readBack;
            READ_CHANNEL.putValCallback( readBack, listener );
        }
    }


    /** update the readback from the setpoint, noise and static error */
    public void updateReadback( final Map<Channel, Double> noiseMap, final Map<Channel, Double> staticErrorMap, final PutListener listener ) throws Exception {
        if ( READ_CHANNEL != null && SET_CHANNEL != null ) {
            final double setPoint;
            setPoint = SET_CHANNEL.getValDbl();
            updateReadback( setPoint, noiseMap, staticErrorMap, listener );
        }
    }

    /**
     * When two channel sets are connected through a
     * conversion factor, update these (do this before
     * updating readback/set records)
     */
    public void updateConversion() {
        if ( READ_CHANNEL != null && SET_CHANNEL != null ) {
            try {
                final double setPoint1 = SET_CHANNEL.getValDbl();
                final double setPoint2 = READ_CHANNEL.getValDbl();
                if (setPoint1 == _lastSetpoint && setPoint2  != _lastReadback) {
                    _lastSetpoint = setPoint2 / _conversionFactor;
                    _lastReadback = setPoint2;
                }
                else if (setPoint1 != _lastSetpoint && setPoint2 == _lastReadback) {
                    _lastSetpoint = setPoint1;
                    _lastReadback = setPoint1 * _conversionFactor;
                }
                else{
                    _lastSetpoint = setPoint1;
                    _lastReadback = setPoint2;
                    return;
                }
                SET_CHANNEL.putVal(_lastSetpoint);
                READ_CHANNEL.putVal(_lastReadback);
            } catch (PutException | GetException e) {
                e.printStackTrace();
            }
        }

    }
}
