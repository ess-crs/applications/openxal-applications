# Open XAL applications

This repository contains all Open XAL applications that arrived to a stage at which they don't require an active development. It is encouraged that applications under development have their own repository to simplify the release of new versions.

## Project structure
The project is configured as a Maven multi-module project that builds and deploy all applications to Artifactory.

Deployment in the control room is done using Ansible, which takes a reduced set of applications.


## See also
You can find the Open XAL core source code at https://gitlab.esss.lu.se/ess-crs/openxal .

## Test against latest core snapshot

By default the applications are built against latest release of core. To update the pom files to build against latest snapshot of the core library instead, run the command

```bash
mvn versions:update-parent -DallowSnapshots=true
```

## Build Results

Branch | GitLab-CI
------ | ------
Open XAL ESS Applications Master | [![ESS Build GitLab-CI](https://gitlab.esss.lu.se/ess-crs/openxal-applications/badges/site.ess.master/pipeline.svg)](https://gitlab.esss.lu.se/ess-crs/openxal-applications/)
