/*
 * Copyright (C) 2017 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.configurator;

import com.cosylab.epics.caj.CAJContext;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Window;
import xal.ca.Channel;
import xal.extension.fxapplication.Controller;
import xal.extension.fxapplication.FxApplication;
import xal.extension.logbook.Logbook;
import xal.extension.logbook.LogbookException;
import xal.extension.logbook.LogbookProvider;
import xal.plugin.epics7.Epics7ChannelSystem;
import xal.tools.apputils.Preferences;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class FXMLController extends Controller {

    private RBAC rbac;
    private OpticsSwitcher opticsSwitcher;

    @FXML
    private CheckBox rbacCheckBox;
    @FXML
    private ListView<String> opticsListView;
    @FXML
    private Button changeOpticsButton;
    @FXML
    private Button setDefaultButton;
    @FXML
    private TextField opticsPathTextField;
    @FXML
    private Button refreshButton;
    @FXML
    private Button revertButton;
    @FXML
    private TextField elogServerTextField;
    @FXML
    private Button changeElogServerButton;
    @FXML
    private TextField pendIOTimeoutTextField;
    @FXML
    private TextField pendEventTimeoutTextField;

    // Property names
    private static final String DEF_TIME_IO = "c_dblDefTimeIO";
    private static final String DEF_TIME_EVENT = "c_dblDefTimeEvent";
      private static final    String DEF_PROTOCOL = "defProtocol";
      
    private double m_dblTmIO;
    private double m_dblTmEvt;
    @FXML
    private TextField CAAddrListTextField;
    @FXML
    private TextField PVAAddrListTextField;
    @FXML
    private Button epicsSaveButton;
    @FXML
    private Button epicsRevertButton;
    @FXML
    private CheckBox caAddCheckBox;
    @FXML
    private CheckBox pvaAddCheckBox;
    @FXML
    private ChoiceBox<String> logbookProviderCB;

    private List<LogbookProvider> logbookProviders;
    @FXML
    private CheckBox defaultProviderCB;
    @FXML
    private TextField defaultLogbookTF;
    @FXML
    private Button setDefaultLogbookB;
    @FXML
    private TextField defaultSavePathTextField;
    @FXML
    private Button changeDefaultSavePathButton;
    @FXML
    private ComboBox<String> defaultProtocolCB;
    @FXML
    private VBox epicsAddrListVBox;
    @FXML
    private HBox caAddrListHBox;
    @FXML
    private HBox pvaAddrListHBox;
    @FXML
    private HBox autoAddrListHBox;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // Initialize RBAC object and associated UI elements
            rbac = new RBAC(rbacCheckBox);
            rbac.updateStatus();
            rbacCheckBox.setSelected(rbac.isEnabled());

            // Initialize OpticsSwitcher object and associated UI elements
            opticsSwitcher = OpticsSwitcher.getInstance();
            if (opticsSwitcher.getOpticsLibraryPath() != null) {
                opticsPathTextField.setText(opticsSwitcher.getOpticsLibraryPath());
                refreshButtonHandler(null);
            } else {
                opticsPathTextField.setText("DEFAULT OPTICS NOT SET!");
            }

            // Default save path
            java.util.prefs.Preferences defaults = Preferences.nodeForPackage(FxApplication.class);
            String defaultSaveDir = defaults.get(FxApplication.SAVE_PATH_PREF_KEY, null);
            if (defaultSaveDir != null && !"".equals(defaultSaveDir)) {
                defaultSavePathTextField.setText(defaultSaveDir);
            }

            // Logbook
            logbookProviders = Logbook.getLogbookProviders();
            ObservableList<String> logbooks = FXCollections.observableList(new ArrayList<>());
            for (LogbookProvider provider : logbookProviders) {
                logbooks.add(provider.getClass().getSimpleName());
            }

            // Hook to update the server url of the selected logbook provider,
            // the checkbox that indicates if it is the default provider,
            // and the default logbook.
            logbookProviderCB.getSelectionModel().selectedIndexProperty().addListener((ov, t, t1) -> {
                elogServerTextField.setText(logbookProviders.get((int) t1).getDefaultServerUrl());
                defaultLogbookTF.setText(logbookProviders.get((int) t1).getDefaultLogbook());
                try {
                    if (Logbook.getDefaultLogbookProvider(false) == null) {
                        defaultProviderCB.setSelected(false);
                    } else {
                        if (logbookProviders.get((int) t1).getClass().equals(Logbook.getDefaultLogbookProvider().getClass())) {
                            defaultProviderCB.setSelected(true);
                        } else {
                            defaultProviderCB.setSelected(false);
                        }
                    }
                } catch (LogbookException ex) {
                    Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });

            logbookProviderCB.setItems(logbooks);

            // Select the default provider or the first option if no default is defined.
            if (Logbook.getDefaultLogbookProvider(false) == null) {
                logbookProviderCB.getSelectionModel().selectFirst();
            } else {
                for (LogbookProvider provider : logbookProviders) {
                    if (provider.getClass().equals(Logbook.getDefaultLogbookProvider().getClass())) {
                        logbookProviderCB.getSelectionModel().select(provider.getClass().getSimpleName());
                        defaultProviderCB.setSelected(true);
                    }
                }
            }

            // Hook to bind checkbox status and Java Preference
            defaultProviderCB.selectedProperty().addListener((ov, t, t1) -> {
                LogbookProvider selectedLogbookProvider = logbookProviders.get(logbookProviderCB.getSelectionModel().getSelectedIndex());
                try {

                    if (t1 == false && selectedLogbookProvider.getClass().equals(Logbook.getDefaultLogbookProvider().getClass())) {
                        try {
                            Logbook.setDefaultLogbookProvider(null);
                        } catch (LogbookException ex) {
                            Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }

                    if (t1 == true && (Logbook.getDefaultLogbookProvider(false) == null || !selectedLogbookProvider.getClass().equals(Logbook.getDefaultLogbookProvider(false).getClass()))) {
                        Logbook.setDefaultLogbookProvider(selectedLogbookProvider);
                    }
                } catch (LogbookException ex) {
                    Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });

            // Initialize EPICS configuration.
            ObservableList<String> epicsProtocolOptions = FXCollections.observableList(Arrays.asList(new String[]{"NONE","CA","PVA"}));
            defaultProtocolCB.setItems(epicsProtocolOptions);
    
            refreshEpicsSettings();

            getAddrList();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void refreshEpicsSettings() {
        // Load default timeouts from preferences if available, otherwise use hardcoded values.
        java.util.prefs.Preferences defaults = Preferences.nodeForPackage(Channel.class);
        
        String defProtocol = defaults.get(DEF_PROTOCOL, "NONE");
        defaultProtocolCB.getSelectionModel().select(defProtocol);
        
        m_dblTmIO = defaults.getDouble(DEF_TIME_IO, -1);
        m_dblTmEvt = defaults.getDouble(DEF_TIME_EVENT, -1);

        if (m_dblTmIO != -1) {
            pendIOTimeoutTextField.setText(Double.toString(m_dblTmIO));
        } else {
            pendIOTimeoutTextField.setText("");
        }

        if (m_dblTmEvt != -1) {
            pendEventTimeoutTextField.setText(Double.toString(m_dblTmEvt));
        } else {
            pendEventTimeoutTextField.setText("");
        }
    }

    @FXML
    private void rbacCheckBoxAction(ActionEvent event) {
        if (rbacCheckBox.isSelected()) {
            rbac.enable();
        } else {
            rbac.disable();
        }
        rbac.updateStatus();
    }

    @FXML
    private void changeOpticsButtonHandler(ActionEvent event) {
        String newOpticsPath;
        newOpticsPath = opticsSwitcher.setDefaultOpticsPathDialog(changeOpticsButton.getScene(),
                opticsPathTextField.getText());
        if (newOpticsPath != null) {
            opticsPathTextField.setText(newOpticsPath);
            refreshButtonHandler(event);
        }
    }

    @FXML
    private void setDefaultButtonHandler(ActionEvent event) {
        boolean isSet = opticsSwitcher.setDefaultPath(opticsListView);
        if (isSet) {
            revertButton.setDisable(false);
        }
    }

    @FXML
    private void refreshButtonHandler(ActionEvent event) {
        opticsSwitcher.refreshList(opticsListView);
        if (opticsListView.getItems().isEmpty()) {
            setDefaultButton.setDisable(true);
        } else {
            setDefaultButton.setDisable(false);
        }
        // Refresh the optics path in case it was automatically changed...
        opticsPathTextField.setText(opticsSwitcher.getOpticsLibraryPath());
    }

    @FXML
    private void opticsPathTextFieldKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            if (new File(opticsPathTextField.getText()).exists()) {
                opticsSwitcher.setOpticsLibraryPath(opticsPathTextField.getText());
                refreshButtonHandler(null);
            } else {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Invalid Path");
                alert.setHeaderText("The optics path is not valid.");
                alert.setContentText("Check the path and try again.");

                alert.showAndWait();
            }
        }
    }

    @FXML
    private void revertButtonHandler(ActionEvent event) {
        opticsSwitcher.revertDefaultPath(opticsListView);

        revertButton.setDisable(true);
    }

    @FXML
    private void changeElogServerButtonHandler(ActionEvent event) {
        logbookProviders.get(logbookProviderCB.getSelectionModel().getSelectedIndex()).setDefaultServerUrl(elogServerTextField.getText());
    }

    @FXML
    private void setDefaultLogbookBHandler(ActionEvent event) {
        logbookProviders.get(logbookProviderCB.getSelectionModel().getSelectedIndex()).setDefaultLogbook(defaultLogbookTF.getText());
    }

    @FXML
    private void epicsSaveButtonHandler(ActionEvent event) {
        // Load default timeouts from preferences if available, otherwise use hardcoded values.
        java.util.prefs.Preferences defaults = Preferences.nodeForPackage(Channel.class);

        if (!pendIOTimeoutTextField.getText().isEmpty()) {
            m_dblTmIO = Double.parseDouble(pendIOTimeoutTextField.getText());

            if (m_dblTmIO > 0) {
                defaults.putDouble(DEF_TIME_IO, m_dblTmIO);
            }
        }

        if (!pendEventTimeoutTextField.getText().isEmpty()) {
            m_dblTmEvt = Double.parseDouble(pendEventTimeoutTextField.getText());

            if (m_dblTmEvt > 0) {
                defaults.putDouble(DEF_TIME_EVENT, m_dblTmEvt);
            }
        }
        
        // Set default protocol
        defaults.put(DEF_PROTOCOL, defaultProtocolCB.getSelectionModel().getSelectedItem());
        
        refreshEpicsSettings();

        // Save EPICS address list if neccessary.
        setAddrList();
    }

    private void getAddrList() {
        Epics7ChannelSystem.loadConfig(false);

        String addressList = System.getProperty(CAJContext.class.getName() + ".addr_list");
        boolean autoAddressList = Boolean.parseBoolean(System.getProperty(CAJContext.class.getName() + ".auto_addr_list"));

        String pvaAddressList = System.getProperty("EPICS_PVA_ADDR_LIST");
        boolean pvaAutoAddressList = Boolean.parseBoolean(System.getProperty("EPICS_PVA_AUTO_ADDR_LIST"));

        CAAddrListTextField.setText(addressList);
        PVAAddrListTextField.setText(pvaAddressList);

        caAddCheckBox.setSelected(autoAddressList);
        pvaAddCheckBox.setSelected(pvaAutoAddressList);
    }

    private void setAddrList() {
        java.util.prefs.Preferences defaults = Preferences.nodeForPackage(Channel.class);

        defaults.put("EPICS_CA_ADDR_LIST", CAAddrListTextField.getText());
        defaults.putBoolean("EPICS_CA_AUTO_ADDR_LIST", caAddCheckBox.isSelected());
        defaults.put("EPICS_PVA_ADDR_LIST", PVAAddrListTextField.getText());
        defaults.putBoolean("EPICS_PVA_AUTO_ADDR_LIST", pvaAddCheckBox.isSelected());
    }

    @FXML
    private void epicsRevertButtonHandler(ActionEvent event) {
        java.util.prefs.Preferences defaults = Preferences.nodeForPackage(Channel.class);

        // Unset default protocol
        defaults.put(DEF_PROTOCOL, "NONE");
        
        defaults.remove(DEF_TIME_IO);
        defaults.remove(DEF_TIME_EVENT);

        defaults.remove("EPICS_CA_ADDR_LIST");
        defaults.remove("EPICS_CA_AUTO_ADDR_LIST");
        defaults.remove("EPICS_PVA_ADDR_LIST");
        defaults.remove("EPICS_PVA_AUTO_ADDR_LIST");

        refreshEpicsSettings();
        getAddrList();
    }

    @FXML
    private void changeDefaultSavePathButtonHandler(ActionEvent event) {
        String newPath = defaultSavePathTextField.getText();

        Window window = changeDefaultSavePathButton.getScene().getWindow();

        if (window != null) {
            DirectoryChooser chooser = new DirectoryChooser();

            chooser.setTitle("Select Default Save Path");

            File initialDirectoryFile = new File(newPath);
            if (initialDirectoryFile.exists()) {
                chooser.setInitialDirectory(initialDirectoryFile);
            }

            File choice = chooser.showDialog(window);

            if (choice != null && choice.exists() && choice.canRead()) {
                newPath = choice.getPath();
                defaultSavePathTextField.setText(newPath);
                java.util.prefs.Preferences defaults = Preferences.nodeForPackage(FxApplication.class);
                defaults.put(FxApplication.SAVE_PATH_PREF_KEY, newPath);
            }
        }
    }

    @FXML
    private void defaultSavePathTextFieldKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            if (new File(defaultSavePathTextField.getText()).exists() || "".equals(defaultSavePathTextField.getText())) {
                java.util.prefs.Preferences defaults = Preferences.nodeForPackage(FxApplication.class);
                defaults.put(FxApplication.SAVE_PATH_PREF_KEY, defaultSavePathTextField.getText());
            } else {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Invalid Path");
                alert.setHeaderText("The default save path is not valid.");
                alert.setContentText("Check the path and try again.");

                alert.showAndWait();
            }
        }
    }
}
